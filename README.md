# mini-spinner

Just about as small as I can make a cli spinner

It reverses direction, allows for directional traveler, has dynamic width, and allows for dynamic traveler size.

6 lines.

pep8.

```
width = 10
for pos in __import__("itertools").cycle(range(width*2)):
    way = pos < width
    __import__("sys").stdout.write("\r{F}{t}{T}".format(**{
        str(way)[0]: ' '*(width-(pos % width)), str(not way)[0]: ' '*(pos % width), "t": ["<--", "-->"][way]}))
    __import__("time").sleep(.2)
```

If the carrige return doesn't work and it write on multiple lines, use this version with the ANSI escape code:

```
width = 10
for pos in __import__("itertools").cycle(range(width*2)):
    way = pos < width
    __import__("sys").stdout.write("\r\x1b[K{F}{t}{T}".format(**{
        str(way)[0]: ' '*(width-(pos % width)), str(not way)[0]: ' '*(pos % width), "t": ["<--", "-->"][way]}))
    __import__("time").sleep(.2)
```
